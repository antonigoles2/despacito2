Click==7.0
Flask==1.0.2
image-slicer==0.2.0
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.1.0
Pillow==5.3.0
Werkzeug==0.14.1
